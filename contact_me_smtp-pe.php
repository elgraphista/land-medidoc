<?php
if($_POST)
{
    $to_Email       = "jvasquez@medidoc.cl"; // Replace with recipient email address
	$subject        = 'Mensaje desde Landing Medidoc '.$_SERVER['SERVER_NAME']; //Subject line for emails
    
    $host           = "smtp.mail.medidoc.com"; // Your SMTP server. For example, smtp.mail.yahoo.com
    $username       = "jvasquez@medidoc.cl"; //For example, your.email@yahoo.com
    $password       = "1234567"; // Your password
    $SMTPSecure     = "ssl"; // For example, ssl
    $port           = 465; // For example, 465
    
    
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    
        //exit script outputting json data
        $output = json_encode(
        array(
            'type'=>'error', 
            'text' => 'Request must come from Ajax'
        ));
        
        die($output);
    } 
    
    //check $_POST vars are set, exit if any missing
    if(!isset($_POST["userName"]) || !isset($_POST["userEmail"]) || !isset($_POST["userMessage"]))
    {
        $output = json_encode(array('type'=>'error', 'text' => 'Input fields are empty!'));
        die($output);
    }

    //Sanitize input data using PHP filter_var().
    $user_Name        = filter_var($_POST["userName"], FILTER_SANITIZE_STRING);
    $user_Email       = filter_var($_POST["userEmail"], FILTER_SANITIZE_EMAIL);
    $user_Phone       = filter_var($_POST["userPhone"], FILTER_SANITIZE_EMAIL);
    $user_Center       = filter_var($_POST["userCenter"], FILTER_SANITIZE_EMAIL);
    $user_Country     = filter_var($_POST["userCountry"], FILTER_SANITIZE_STRING);
    
    $user_Center = str_replace("\&#39;", "'", $user_Center);
    $user_Center = str_replace("&#39;", "'", $user_Center);
    
    //additional php validation
    if(strlen($user_Name)<4) // If length is less than 4 it will throw an HTTP error.
    {
        $output = json_encode(array('type'=>'error', 'text' => '¡El nombre es demasiado corto o está vacío!'));
        die($output);
    }
    if(!filter_var($user_Email, FILTER_VALIDATE_EMAIL)) //email validation
    {
        $output = json_encode(array('type'=>'error', 'text' => '¡Por favor introduzca una dirección de correo electrónico válida!'));
        die($output);
    }
    if(strlen($user_Center)<5) //check emtpy message
    {
        $output = json_encode(array('type'=>'error', 'text' => 'El nombre del centro asistencial es demasiado corto'));
        die($output);
    }
    
    //proceed with PHP email.
    include("php/PHPMailerAutoload.php"); //you have to upload class files "class.phpmailer.php" and "class.smtp.php"
 
	$mail = new PHPMailer();
	 
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
	
	$mail->Host = $host;
	$mail->Username = $username;
	$mail->Password = $password;
	$mail->SMTPSecure = $SMTPSecure;
	$mail->Port = $port;
	
	 
	$mail->setFrom($username);
	$mail->addReplyTo($user_Email);
	 
	$mail->AddAddress($to_Email);
	$mail->Subject = $subject;
	$mail->Body = $user_Message. "\r\n\n"  .'Name: '.$user_Name. "\r\n" .'Email: '.$user_Email;
	$mail->WordWrap = 200;
	$mail->IsHTML(false);

	if(!$mail->send()) {

		$output = json_encode(array('type'=>'error', 'text' => 'El mensaje no pudo ser enviado. Error de envío: ' . $mail->ErrorInfo));
		die($output);

	} else {
	    $output = json_encode(array('type'=>'message', 'text' => '¡Hola '.$user_Name .'! ¡Pronto nos pondremos en contacto contigo!'));
		die($output);
	}
    
}
?>