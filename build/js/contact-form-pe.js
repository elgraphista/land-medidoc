/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
$(document).ready(function(){
    $("#submit_btn").click(function(){
        
        //get input field values
        var user_name = $('input[name=name]').val();
        var user_email = $('input[name=email]').val();
        var user_phone = $('input[name=phone]').val();
        var user_center = $('input[name=medicalCenter]').val();
        var user_country = $('select[name=countryOptions]').val();
        
        //simple validation at client's end
        //we simply change border color to red if empty field using .css()
        var proceed = true;

        if (user_name == "" || user_email == "" || user_phone == "" || user_center == "" || user_country.val == "") {
            $('input').css('border-color', '#e41919');
            $('.message-alert').css('display', 'block');
            proceed = false;
        }
        
        //everything looks good! proceed...
        if (proceed) {
            $('input').css('border-color', '#CCC');
            $('.message-alert').css('display', 'none');
            //data to be sent to server
            post_data = {
                'userName': user_name,
                'userEmail': user_email,
                'userPhone': user_phone,
                'userCenter': user_center,
                'userCountry': user_country.val,
            };
            
            //Ajax post data to server
            $.post('contact_me-pe.php', post_data, function(response){
            
                //load json data from server and output message     
                if (response.type == 'error') {
                    output = '<div class="error">' + response.text + '</div>';
                }
                else {
                
                    output = '<div class="success">' + response.text + '</div>';
                    
                    //reset values in all input fields
                    $('#contact_form input').val('');
                    $('#contact_form select').val('');
                    $('#contact_form textarea').val('');
                }
                
                $("#result").hide().html(output).slideDown();
            }, 'json');
            
        }
        
        return false;
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form input, #contact_form textarea, #contact_form select").keyup(function(){
        $("#contact_form input, #contact_form textarea").css('border-color', '');
        $("#result").slideUp();
    });
    
});
