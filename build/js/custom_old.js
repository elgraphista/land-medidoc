/* Custom JS */

// Radios

const radio1 = document.getElementById("optionsRadios1");
const radio2 = document.getElementById("optionsRadios2");
const radio3 = document.getElementById("optionsRadios3");
const radio4 = document.getElementById("optionsRadios4");

var slider = new Slider("#planRange");
var sliderVal = document.getElementById("planRangeSliderVal");


var contSlider = slider.on("slide", (sliderValue) => {
    sliderVal.textContent = sliderValue;
    if (sliderValue <= 1) {
        planRangeSliderValLabel.textContent = 'profesional';
    } else if (sliderValue == 20) {
        planRangeSliderValLabel.textContent = 'o más profesionales'
    } else {
        planRangeSliderValLabel.textContent = 'profesionales'
    }

    const numberPro = sliderValue;

    const priceMensual = numberPro === 1 ? '$20.000 por mes':
                    numberPro >= 2 && numberPro <= 5 ? '$40.000 por mes':
                    numberPro >= 6 && numberPro <= 10 ? '$60.000 por mes':
                    numberPro >= 11 && numberPro <= 15 ? '$75.000 por mes':
                    numberPro >= 16 && numberPro <= 20 ? '$80.000 por mes': '$20.000 por mes';

    const priceTrimestral = numberPro === 1 ? '$57.000 por trimestre':
                    numberPro >= 2 && numberPro <= 5 ? '$114.000 por trimestre':
                    numberPro >= 6 && numberPro <= 10 ? '$171.000 por trimestre':
                    numberPro >= 11 && numberPro <= 15 ? '$213.750 por trimestre':
                    numberPro >= 16 && numberPro <= 20 ? '$228.000 por trimestre': '$57.000 por trimestre';

    const priceSemestral = numberPro === 1 ? '$102.000 por semestre':
                    numberPro >= 2 && numberPro <= 5 ? '$204.000 por semestre':
                    numberPro >= 6 && numberPro <= 10 ? '$306.000 por semestre':
                    numberPro >= 11 && numberPro <= 15 ? '$382.500 por semestre':
                    numberPro >= 16 && numberPro <= 20 ? '$408.000 por semestre': '$102.000 por semestre';

    const priceAnual = numberPro === 1 ? '$168.000 por año':
                    numberPro >= 2 && numberPro <= 5 ? '$336.000 por año':
                    numberPro >= 6 && numberPro <= 10 ? '$504.000 por año':
                    numberPro >= 11 && numberPro <= 15 ? '$630.000 por año':
                    numberPro >= 16 && numberPro <= 20 ? '$672.000 por año': '$168.000 por año';


    messagePlan.textContent = priceMensual;

/*     radio1.addEventListener('click', () => {
        messagePlan.textContent = priceMensual;
    })
    radio2.addEventListener('click', () => {
        messagePlan.textContent = priceTrimestral;
    })
    radio3.addEventListener('click', () => {
        messagePlan.textContent = priceSemestral;
    })
    radio4.addEventListener('click', () => {
        messagePlan.textContent = priceAnual;
    }) */


    if (radio1.checked == true) {
        messagePlan.textContent = priceMensual;
    }
    else if (radio2.checked == true) {
        messagePlan.textContent = priceTrimestral;
    }
    else if (radio3.checked == true) {
        messagePlan.textContent = priceSemestral;
    }
    else if (radio4.checked == true) {
        messagePlan.textContent = priceAnual;
    }
    else {
        // DO NOTHING
        } 
        
});

console.log(contSlider.element.numberPro);


radio1.addEventListener('click', () => {
    messagePlan.textContent = '$20.000 por mes';
})
radio2.addEventListener('click', () => {
    messagePlan.textContent = '$57.000 por trimestre';
})
radio3.addEventListener('click', () => {
    messagePlan.textContent = '$102.000 por semestre';
})
radio4.addEventListener('click', () => {
    messagePlan.textContent = '$168.000 por año';
})